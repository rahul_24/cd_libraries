package com.cd.compressor

import android.graphics.Bitmap
import java.io.ByteArrayOutputStream


class ImageCompressor {

    fun compressImage(bitmap: Bitmap): ByteArray? {
        var bmpPicByteArray: ByteArray? = null
        var bmpPic = bitmap
        val scaleSize = 1024                //Image higher dimension could be 1024.
        val originalWidth = bmpPic.width
        val originalHeight = bmpPic.height
        //  Log.v("Pictures", "after scaling Width and height are " + originalWidth+ "--" + originalHeight);
        var newWidth = -1
        var newHeight = -1
        val multiFactor: Float
        if (originalHeight > scaleSize || originalWidth > scaleSize) {
            when {
                originalHeight > originalWidth -> {
                    newHeight = scaleSize
                    multiFactor = originalWidth.toFloat() / originalHeight.toFloat()
                    newWidth = (newHeight * multiFactor).toInt()
                }
                originalWidth > originalHeight -> {
                    newWidth = scaleSize
                    multiFactor = originalHeight.toFloat() / originalWidth.toFloat()
                    newHeight = (newWidth * multiFactor).toInt()
                }
                originalHeight == originalWidth -> {
                    newHeight = scaleSize
                    newWidth = scaleSize
                }
            }
            bmpPic = Bitmap.createScaledBitmap(bmpPic, newWidth, newHeight, false)
        }

        val MAX_IMAGE_SIZE = 1024 * 512
        var compressQuality = 104       // quality decreasing by 5% every loop. (start from 99)
        var streamLength = MAX_IMAGE_SIZE + 1
        while (streamLength > MAX_IMAGE_SIZE) {
            val bmpStream = ByteArrayOutputStream()
            compressQuality -= 5
            //  Log.d(TAG, "Quality: " + compressQuality);
            bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream)
            bmpPicByteArray = bmpStream.toByteArray()
            streamLength = bmpPicByteArray.size

            // Log.d(TAG, "Size: " + streamLength/1024);
        }
        return bmpPicByteArray
    }

}