package com.cd.libraries

import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.cd.downloader.*
import java.io.File

class MainActivity : AppCompatActivity() {
    private lateinit var downloadManager: Downloader
    private var myDownloadStatusListener = MyDownloadDownloadStatusListener()
    var downloadId1 = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        downloadManager = Downloader(DOWNLOAD_THREAD_POOL_SIZE)
        val retryPolicy: RetryPolicy = DefaultRetryPolicy()
        val filesDir = getExternalFilesDir("")

        val downloadUri = Uri.parse(FILE1)
        val destinationUri = Uri.parse(filesDir.toString() + "/" + getFileName(FILE1))
        val downloadRequest1 = DownloadRequest(downloadUri)
            .setDestinationURI(destinationUri)
            .setPriority(DownloadRequest.Priority.LOW)
            .setRetryPolicy(retryPolicy)
            .setDownloadContext("Download")
            .setStatusListener(myDownloadStatusListener)

        val btnDownload = findViewById<Button>(R.id.btnDownload)
        btnDownload.setOnClickListener {
            val destUri = Uri.parse(filesDir.toString() + "/" + getFileName(FILE1))
            if (isFileExists(destUri)) {
                Toast.makeText(this, "File already downloaded", Toast.LENGTH_SHORT).show()
                findViewById<ImageView>(R.id.imgDownload).setImageURI(destUri)
                return@setOnClickListener
            }
            if (downloadManager.query(downloadId1) == IDownloader.STATUS_NOT_FOUND) {
                downloadId1 = downloadManager.add(downloadRequest1)
            }
        }
    }

    inner class MyDownloadDownloadStatusListener : DownloadStatusListener {
        override fun onDownloadComplete(downloadRequest: DownloadRequest) {
            when (val id = downloadRequest.downloadId) {
                //downloadId1 -> { mProgress1Txt!!.text = downloadRequest.downloadContext.toString() + " id: " + id + " Completed" }
            }
        }

        override fun onDownloadFailed(
            downloadRequest: DownloadRequest,
            errorCode: Int,
            errorMessage: String
        ) {
            when (val id = downloadRequest.downloadId) {
                downloadId1 -> {
                    //mProgress1Txt!!.text = "Download id: $id Failed: ErrorCode $errorCode, $errorMessage"
                    //mProgress1!!.progress = 0
                }
            }
        }

        override fun onProgress(
            downloadRequest: DownloadRequest,
            totalBytes: Long,
            downloadedBytes: Long,
            progress: Int
        ) {
            val id = downloadRequest.downloadId
            println("######## onProgress ###### $id : $totalBytes : $downloadedBytes : $progress")
            when (id) {
                downloadId1 -> {
                    //mProgress1Txt!!.text = "Download id: $id, $progress%  " + getBytesDownloaded(progress, totalBytes)
                    //mProgress1!!.progress = progress
                }
            }
        }
    }

    private fun isFileExists(destinationUri: Uri): Boolean {
        return File(destinationUri.path!!).isFile
    }

    private fun getFileName(url: String): String {
        return url.substring(url.lastIndexOf('/') + 1, url.length).substringBefore("?")
    }

    companion object {
        private const val DOWNLOAD_THREAD_POOL_SIZE = 4
        private const val FILE1 =
            "https://i.picsum.photos/id/120/200/300.jpg?hmac=MG0pF8PmFWKEzuFPIWwUw15CadARgBh6cxPh_YuAALY"
    }
}