package com.cd.downloader

import android.os.Handler
import android.os.Looper
import com.cd.downloader.util.Log.e
import java.security.InvalidParameterException
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.PriorityBlockingQueue
import java.util.concurrent.atomic.AtomicInteger

class DownloadRequestQueue {
    /**
     * The set of all requests currently being processed by this RequestQueue. A Request will be in this set if it is waiting in any queue or currently being processed by any dispatcher.
     */
    private var mCurrentRequests: MutableSet<DownloadRequest>? = HashSet()

    /** The queue of requests that are actually going out to the network.  */
    private var mDownloadQueue: PriorityBlockingQueue<DownloadRequest>? = PriorityBlockingQueue()

    /** The download dispatchers  */
    private var mDownloadDispatchers: Array<DownloadDispatcher?>? = null

    /** Used for generating monotonically-increasing sequence numbers for requests.  */
    private val mSequenceGenerator = AtomicInteger()
    private var mDelivery: CallBackDelivery? = null

    /**
     * Delivery class to delivery the call back to call back registrar in main thread.
     */
    internal inner class CallBackDelivery(handler: Handler) {
        /** Used for posting responses, typically to the main thread. (i.e MainThreadExecutor)  */
        private val mCallBackExecutor: Executor = Executor { runnable -> handler.post(runnable) }
        fun postDownloadComplete(request: DownloadRequest) {
            mCallBackExecutor.execute { request.statusListener?.onDownloadComplete(request) }
        }

        fun postDownloadFailed(request: DownloadRequest, errorCode: Int, errorMsg: String) {
            mCallBackExecutor.execute {
                request.statusListener?.onDownloadFailed(
                    request,
                    errorCode,
                    errorMsg
                )
            }
        }

        fun postProgressUpdate(
            request: DownloadRequest,
            totalBytes: Long,
            downloadedBytes: Long,
            progress: Int
        ) {
            mCallBackExecutor.execute {
                request.statusListener?.onProgress(
                    request,
                    totalBytes,
                    downloadedBytes,
                    progress
                )
            }
        }
    }

    /**
     * Default constructor.
     */
    constructor() {
        initialize(Handler(Looper.getMainLooper()))
    }

    /**
     * Creates the download dispatchers workers pool.
     *
     * Deprecated:
     */
    constructor(threadPoolSize: Int) {
        initialize(Handler(Looper.getMainLooper()), threadPoolSize)
    }

    /**
     * Construct with provided callback handler.
     *
     * @param callbackHandler
     */
    constructor(callbackHandler: Handler?) {
        if (callbackHandler == null) {
            throw InvalidParameterException("callbackHandler must not be null")
        }
        initialize(callbackHandler)
    }

    fun start() {
        stop() // Make sure any currently running dispatchers are stopped.

        // Create download dispatchers (and corresponding threads) up to the pool size.
        mDownloadDispatchers!!.indices.forEach { i ->
            val downloadDispatcher = DownloadDispatcher(mDownloadQueue, mDelivery)
            mDownloadDispatchers!![i] = downloadDispatcher
            downloadDispatcher.start()
        }
    }
    // Package-Private methods.
    /**
     * Generates a download id for the request and adds the download request to the
     * download request queue for the dispatchers pool to act on immediately.
     * @param request
     * @return downloadId
     */
    fun add(request: DownloadRequest): Int {
        val downloadId = downloadId
        // Tag the request as belonging to this queue and add it to the set of current requests.
        request.setDownloadRequestQueue(this)
        synchronized(mCurrentRequests!!) { mCurrentRequests!!.add(request) }

        // Process requests in the order they are added.
        request.downloadId = downloadId
        mDownloadQueue!!.add(request)
        return downloadId
    }

    /**
     * Returns the current download state for a download request.
     *
     * @param downloadId
     * @return
     */
    fun query(downloadId: Int): Int {
        synchronized(mCurrentRequests!!) {
            return mCurrentRequests!!.find { it.downloadId == downloadId }?.downloadState
                ?: IDownloader.STATUS_NOT_FOUND
        }
    }

    /**
     * Cancel all the dispatchers in work and also stops the dispatchers.
     */
    fun cancelAll() {
        synchronized(mCurrentRequests!!) {
            mCurrentRequests!!.forEach { request -> request.cancel() }

            // Remove all the requests from the queue.
            mCurrentRequests!!.clear()
        }
    }

    /**
     * Cancel a particular download in progress. Returns 1 if the download Id is found else returns 0.
     *
     * @param downloadId
     * @return int
     */
    fun cancel(downloadId: Int): Int {
        synchronized(mCurrentRequests!!) {
            return mCurrentRequests!!.find { it.downloadId == downloadId }?.let {
                it.cancel()
                1
            } ?: 0
        }
    }

    /**
     * Pause a particular download in progress.
     *
     * @param downloadId - selected download request Id
     * @return It will return 1 if the download Id is found else returns 0.
     */
    fun pause(downloadId: Int): Int {
        checkResumableDownloadEnabled(downloadId)
        return cancel(downloadId)
    }

    /**
     * Pause all the dispatchers in work and also cancel and stops the dispatchers.
     */
    fun pauseAll() {
        checkResumableDownloadEnabled(-1) // Error code -1 handle for cancelAll()
        cancelAll()
    }

    /**
     * This is called by methods that want to throw an exception if the [DownloadRequest]
     * hasn't enable isResumable feature.
     */
    private fun checkResumableDownloadEnabled(downloadId: Int) {
        synchronized(mCurrentRequests!!) {
            mCurrentRequests!!.forEach { request ->
                if (downloadId == -1 && !request.isResumable) {
                    e(
                        "DownloadManagerPro",
                        String.format(
                            Locale.getDefault(),
                            "This request has not enabled resume feature hence request will be cancelled. Request Id: %d",
                            request.downloadId
                        )
                    )
                } else //ignored, It can not be a scenario to happen.
                    check(!(request.downloadId == downloadId && !request.isResumable)) { "You cannot pause the download, unless you have enabled Resume feature in DownloadRequest." }
                //ignored, It can not be a scenario to happen.
            }
        }
    }

    fun finish(request: DownloadRequest) {
        if (mCurrentRequests != null) { //if finish and release are called together it throws NPE
            // Remove from the queue.
            synchronized(mCurrentRequests!!) { mCurrentRequests!!.remove(request) }
        }
    }

    /**
     * Cancels all the pending & running requests and releases all the dispatchers.
     */
    fun release() {
        if (mCurrentRequests != null) {
            synchronized(mCurrentRequests!!) {
                mCurrentRequests!!.clear()
                mCurrentRequests = null
            }
        }
        if (mDownloadQueue != null) {
            mDownloadQueue = null
        }
        if (mDownloadDispatchers != null) {
            stop()
            mDownloadDispatchers!!.indices.forEach { i -> mDownloadDispatchers!![i] = null }
            mDownloadDispatchers = null
        }
    }
    // Private methods.
    /**
     * Perform construction.
     *
     * @param callbackHandler
     */
    private fun initialize(callbackHandler: Handler) {
        val processors = Runtime.getRuntime().availableProcessors()
        mDownloadDispatchers = arrayOfNulls(processors)
        mDelivery = CallBackDelivery(callbackHandler)
    }

    /**
     * Perform construction with custom thread pool size.
     */
    private fun initialize(callbackHandler: Handler, threadPoolSize: Int) {
        mDownloadDispatchers = arrayOfNulls(threadPoolSize)
        mDelivery = CallBackDelivery(callbackHandler)
    }

    /**
     * Stops download dispatchers.
     */
    private fun stop() {
        mDownloadDispatchers!!.indices.forEach { i -> mDownloadDispatchers!![i]?.quit() }
    }

    /**
     * Gets a sequence number.
     */
    private val downloadId: Int
        get() = mSequenceGenerator.incrementAndGet()
}