package com.cd.downloader.util

import android.util.Log

object Log {
    private const val sTag = "DownloadManagerPro"
    var isLoggingEnabled = false

    fun v(msg: String?): Int {
        return if (isLoggingEnabled) {
            Log.v(sTag, msg!!)
        } else 0
    }

    fun v(tag: String?, msg: String?): Int {
        return if (isLoggingEnabled) {
            Log.v(tag, msg!!)
        } else 0
    }

    fun v(msg: String?, tr: Throwable?): Int {
        return if (isLoggingEnabled) {
            Log.v(sTag, msg, tr)
        } else 0
    }

    fun v(tag: String?, msg: String?, tr: Throwable?): Int {
        return if (isLoggingEnabled) {
            Log.v(tag, msg, tr)
        } else 0
    }

    fun d(msg: String?): Int {
        return if (isLoggingEnabled) {
            Log.d(sTag, msg!!)
        } else 0
    }

    fun d(tag: String?, msg: String?): Int {
        return if (isLoggingEnabled) {
            Log.d(tag, msg!!)
        } else 0
    }

    fun d(msg: String?, tr: Throwable?): Int {
        return if (isLoggingEnabled) {
            Log.d(sTag, msg, tr)
        } else 0
    }

    fun d(tag: String?, msg: String?, tr: Throwable?): Int {
        return if (isLoggingEnabled) {
            Log.d(tag, msg, tr)
        } else 0
    }

    fun i(msg: String?): Int {
        return if (isLoggingEnabled) {
            Log.i(sTag, msg!!)
        } else 0
    }

    fun i(tag: String?, msg: String?): Int {
        return if (isLoggingEnabled) {
            Log.i(tag, msg!!)
        } else 0
    }

    fun i(msg: String?, tr: Throwable?): Int {
        return if (isLoggingEnabled) {
            Log.i(sTag, msg, tr)
        } else 0
    }

    fun i(tag: String?, msg: String?, tr: Throwable?): Int {
        return if (isLoggingEnabled) {
            Log.i(tag, msg, tr)
        } else 0
    }

    fun w(msg: String?): Int {
        return if (isLoggingEnabled) {
            Log.w(sTag, msg!!)
        } else 0
    }

    fun w(tag: String?, msg: String?): Int {
        return if (isLoggingEnabled) {
            Log.w(tag, msg!!)
        } else 0
    }

    fun w(msg: String?, tr: Throwable?): Int {
        return if (isLoggingEnabled) {
            Log.w(sTag, msg, tr)
        } else 0
    }

    fun w(tag: String?, msg: String?, tr: Throwable?): Int {
        return if (isLoggingEnabled) {
            Log.w(tag, msg, tr)
        } else 0
    }

    fun e(msg: String?): Int {
        return if (isLoggingEnabled) {
            Log.e(sTag, msg!!)
        } else 0
    }

    fun e(tag: String?, msg: String?): Int {
        return if (isLoggingEnabled) {
            Log.e(tag, msg!!)
        } else 0
    }

    fun e(msg: String?, tr: Throwable?): Int {
        return if (isLoggingEnabled) {
            Log.e(sTag, msg, tr)
        } else 0
    }

    fun e(tag: String?, msg: String?, tr: Throwable?): Int {
        return if (isLoggingEnabled) {
            Log.e(tag, msg, tr)
        } else 0
    }

    fun t(msg: String?, vararg args: Any?): Int {
        return if (isLoggingEnabled) {
            Log.v("test", String.format(msg!!, *args))
        } else 0
    }
}