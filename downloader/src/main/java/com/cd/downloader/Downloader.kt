package com.cd.downloader

import android.content.Context
import android.net.Uri
import android.os.Handler
import android.webkit.MimeTypeMap
import com.cd.downloader.util.Log
import java.io.File
import java.util.*

/**
 * This class used to handles long-running HTTP downloads, User can raise a [DownloadRequest] request with multiple features.
 * The download manager will conduct the download in the background, taking care of HTTP interactions, failures  and retrying downloads
 * across connectivity changes.
 * Read this article- thread pool executors, and their use within Android
 * https://blog.mindorks.com/threadpoolexecutor-in-android-8e9d22330ee3
 */
class Downloader : IDownloader {
    /**
     * Download request queue takes care of handling the request based on priority.
     */
    private var mRequestQueue: DownloadRequestQueue?
    /**
     * Construct with logging Enabled.
     * @param loggingEnabled - enable log info
     */
    /**
     * Default constructor
     */
    @JvmOverloads
    constructor(loggingEnabled: Boolean = true) {
        mRequestQueue = DownloadRequestQueue()
        mRequestQueue!!.start()
        setLoggingEnabled(loggingEnabled)
    }

    /**
     * Construct with provided callback handler
     *
     * @param callbackHandler - callback handler
     */
    constructor(callbackHandler: Handler?) {
        mRequestQueue = DownloadRequestQueue(callbackHandler)
        mRequestQueue!!.start()
        setLoggingEnabled(true)
    }

    /**
     * Constructor taking MAX THREAD POOL SIZE.
     */
    constructor(threadPoolSize: Int) {
        mRequestQueue = DownloadRequestQueue(threadPoolSize)
        mRequestQueue!!.start()
        setLoggingEnabled(true)
    }

    /**
     * Constructor taking download url List. MAX THREAD POOL SIZE will be equal to size of this url List.
     * Starts downloading immediately.
     * @return  <code>download id</code> if file does not exist, adds download request in the queue;
     *          <code>uri path</code> otherwise.
     *
     * @deprecated use Default Constructor. As the thread pool size will not respected anymore through this constructor.
     * Thread pool size is determined with the number of available processors on the device.
     */
    constructor(context: Context,
                urlList: List<String>,
                downloadStatusListener: DownloadStatusListener? = null,
                downloadContext : String? = null,
                downloadResumable : Boolean = false,
                forceDownload : Boolean = false) {

        mRequestQueue = DownloadRequestQueue(urlList.size)
        mRequestQueue!!.start()
        setLoggingEnabled(true)
        addAll(context, urlList, downloadStatusListener, downloadContext, downloadResumable, forceDownload)
    }

    /**
     * Add a new download. The download will start automatically once the download manager is
     * ready to execute it and connectivity is available.
     *
     * @param request the parameters specifying this download
     * @return an ID for the download, unique across the application.  This ID is used to make future
     * calls related to this download.
     * @throws IllegalArgumentException
     */
    @Throws(IllegalArgumentException::class)
    override fun add(request: DownloadRequest): Int {
        checkReleased("add(...) called on a released DownloadManagerPro.")
        requireNotNull(request) { "DownloadRequest cannot be null" }
        return mRequestQueue!!.add(request)
    }

    override fun cancel(downloadId: Int): Int {
        checkReleased("cancel(...) called on a released DownloadManagerPro.")
        return mRequestQueue!!.cancel(downloadId)
    }

    override fun cancelAll() {
        checkReleased("cancelAll() called on a released DownloadManagerPro.")
        mRequestQueue!!.cancelAll()
    }

    override fun pause(downloadId: Int): Int {
        checkReleased("pause(...) called on a released DownloadManagerPro.")
        return mRequestQueue!!.pause(downloadId)
    }

    override fun pauseAll() {
        checkReleased("pauseAll() called on a released DownloadManagerPro.")
        mRequestQueue!!.pauseAll()
    }

    override fun query(downloadId: Int): Int {
        checkReleased("query(...) called on a released DownloadManagerPro.")
        return mRequestQueue!!.query(downloadId)
    }

    override fun release() {
        if (!isReleased) {
            mRequestQueue!!.release()
            mRequestQueue = null
        }
    }

    override val isReleased: Boolean
        get() = mRequestQueue == null

    /**
     * This is called by methods that want to throw an exception if the DownloadManager
     * has already been released.
     */
    private fun checkReleased(errorMessage: String) {
        check(!isReleased) { errorMessage }
    }

    fun getBytesDownloaded(progress: Int, totalBytes: Long): String {
        //Greater than 1 MB
        val bytesCompleted = progress * totalBytes / 100
        if (totalBytes >= 1000000) {
            return "" + String.format(
                "%.1f",
                bytesCompleted.toFloat() / 1000000
            ) + "/" + String.format("%.1f", totalBytes.toFloat() / 1000000) + "MB"
        }
        return if (totalBytes >= 1000) {
            "" + String.format(
                "%.1f",
                bytesCompleted.toFloat() / 1000
            ) + "/" + String.format("%.1f", totalBytes.toFloat() / 1000) + "Kb"
        } else {
            "$bytesCompleted/$totalBytes"
        }
    }

    fun addAll(context: Context,
               urlList: List<String>,
               downloadStatusListener: DownloadStatusListener? = null,
               downloadContext : String? = null,
               downloadResumable : Boolean = false,
               forceDownload : Boolean = false) : List<Any> {

        return urlList.map { url ->
            add(context,
                url,
                downloadStatusListener,
                downloadContext,
                downloadResumable,
                forceDownload
            )
        }
    }

    fun add(context: Context,
            url: String,
            downloadStatusListener: DownloadStatusListener? = null,
            downloadContext : String? = null,
            downloadResumable : Boolean = false,
            forceDownload : Boolean = false) : Any {

        val retryPolicy: RetryPolicy = DefaultRetryPolicy()

        val filesDir = context.getExternalFilesDir("") ?: return ""
        val fileName = getFileNameFromUrl(url)
        val destUri = Uri.parse("$filesDir/$fileName")

        val result = if (forceDownload || !isFileExists(context, url)) {
            val downloadUri = Uri.parse(url)
            val downloadRequest = DownloadRequest(downloadUri)
                .setDestinationURI(destUri)
                .setPriority(DownloadRequest.Priority.IMMEDIATE)
                .setRetryPolicy(retryPolicy)
                .setDownloadContext(downloadContext ?: url)
                .setDownloadResumable(downloadResumable)
                .setStatusListener(downloadStatusListener)
            add(downloadRequest)
        } else destUri

        return result
    }

    private fun isFileExists(context: Context, url: String): Boolean {
        val filesDir = context.getExternalFilesDir("") ?: return false
        val fileName = getFileNameFromUrl(url)

        return File("$filesDir/$fileName").isFile
    }

    fun isFileExists(uri: Uri): Boolean {
        return File(uri.path!!).isFile
    }

    private fun getMimeType(url: String?): String {
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension.isNullOrEmpty()) {
            return "jpg"
        }
        return extension
    }

    private fun getUniqueName(url: String?): String{
        val tUrl = UUID.nameUUIDFromBytes(url!!.toByteArray()).toString()
        return tUrl
    }

    fun getFileNameFromUrl(url: String): String {
        return getUniqueName(url) + "." + getMimeType(url)
    }

    companion object {
        private fun setLoggingEnabled(enabled: Boolean) {
            Log.isLoggingEnabled = enabled
        }
    }
}