package com.cd.downloader

import android.net.Uri
import com.cd.downloader.DownloadRequest.Priority
import java.util.*

/**
 * This class used to handle user requests and provides priorities to the request even there may be 'n' number of request raised.
 * Basically this is [Comparable] class and It compares the [DownloadRequest]'s [Priority] levels and react accordingly.
 */
class DownloadRequest(uri: Uri?) : Comparable<DownloadRequest> {
    /**
     * Priority values.  Requests will be processed from higher priorities to
     * lower priorities, in FIFO order.
     */
    enum class Priority {
        LOW, NORMAL, HIGH, IMMEDIATE
    }

    /**
     * Tells the current download state of this request
     */
    var downloadState: Int
    /**
     * Gets the download id.
     *
     * @return the download id
     */
    /**
     * Sets the download Id of this request.  Used by [DownloadRequestQueue].
     */
    /**
     * Download Id assigned to this request
     */
    var downloadId = 0

    /**
     * The URI resource that this request is to download
     */
    var uri: Uri

    /**
     * The destination path on the device where the downloaded files needs to be put
     * It can be either External Directory ( SDcard ) or
     * internal app cache or files directory.
     * For using external SDCard access, application should have
     * this permission android.permission.WRITE_EXTERNAL_STORAGE declared.
     */
    lateinit var destinationURI: Uri

    private var mRetryPolicy: RetryPolicy? = null
    /**
     * Returns true if this request has been canceled.
     */
    /**
     * Whether or not this request has been canceled.
     */
    var isCancelled = false
    var deleteDestinationFileOnFailure = true
    private var mRequestQueue: DownloadRequestQueue? = null

    /**
     * Gets the status listener. For internal use.
     *
     * @return  the status listener
     */
    var statusListener: DownloadStatusListener? = null

    var downloadContext: Any? = null

    /**
     * Returns all custom headers set by user
     *
     * @return
     */
    val customHeaders: HashMap<String, String>

    /**
     * Returns the [Priority] of this request; [Priority.NORMAL] by default.
     */
    var priority = Priority.NORMAL

    var isResumable = false


    /**
     * Set the [Priority]  of this request;
     *
     * @param priority
     * @return request
     */
    fun setPriority(priority: Priority): DownloadRequest {
        this.priority = priority
        return this
    }

    /**
     * Adds custom header to request
     *
     * @param key
     * @param value
     */
    fun addCustomHeader(key: String, value: String): DownloadRequest {
        customHeaders[key] = value
        return this
    }

    /**
     * Associates this request with the given queue. The request queue will be notified when this
     * request has finished.
     */
    fun setDownloadRequestQueue(downloadQueue: DownloadRequestQueue?) {
        mRequestQueue = downloadQueue
    }

    val retryPolicy: RetryPolicy
        get() = mRetryPolicy ?: DefaultRetryPolicy()

    fun setRetryPolicy(mRetryPolicy: RetryPolicy): DownloadRequest {
        this.mRetryPolicy = mRetryPolicy
        return this
    }

    /**
     * Sets the status listener for this download request. Download manager sends progress,
     * failure and completion updates to this listener for this download request.
     *
     * @param downloadStatusListener the status listener for this download
     */
    fun setStatusListener(downloadStatusListener: DownloadStatusListener?): DownloadRequest {
        statusListener = downloadStatusListener
        return this
    }

    fun setDownloadContext(downloadContext: Any?): DownloadRequest {
        this.downloadContext = downloadContext
        return this
    }

    fun setUri(mUri: Uri): DownloadRequest {
        uri = mUri
        return this
    }

    fun setDestinationURI(destinationURI: Uri): DownloadRequest {
        this.destinationURI = destinationURI
        return this
    }

    /**
     * It marks the request with resumable feature and It is an optional feature
     * @param isDownloadResumable - It enables resumable feature for this request
     * @return - current [DownloadRequest]
     */
    fun setDownloadResumable(isDownloadResumable: Boolean): DownloadRequest {
        isResumable = isDownloadResumable
        setDeleteDestinationFileOnFailure(false) // If resumable feature enabled, downloaded file should not be deleted.
        return this
    }

    /**
     * Set if destination file should be deleted on download failure.
     * Use is optional: default is to delete.
     */
    fun setDeleteDestinationFileOnFailure(deleteOnFailure: Boolean): DownloadRequest {
        deleteDestinationFileOnFailure = deleteOnFailure
        return this
    }

    /**
     * Mark this request as canceled.  No callback will be delivered.
     */
    fun cancel() {
        isCancelled = true
    }
    //Package-private methods.
    /**
     * Marked the request as canceled is aborted.
     */
    fun abortCancel() {
        isCancelled = false
    }

    fun finish() {
        mRequestQueue!!.finish(this)
    }

    override fun compareTo(other: DownloadRequest): Int {
        val left = priority
        val right = other.priority

        // High-priority requests are "lesser" so they are sorted to the front.
        // Equal priorities are sorted by sequence number to provide FIFO ordering.
        return if (left == right) downloadId - other.downloadId else right.ordinal - left.ordinal
    }

    init {
        if (uri == null) {
            throw NullPointerException()
        }
        val scheme = uri.scheme
        require(!(scheme == null || scheme != "http" && scheme != "https")) { "Can only download HTTP/HTTPS URIs: $uri" }
        customHeaders = HashMap()
        downloadState = IDownloader.STATUS_PENDING
        this.uri = uri
    }
}