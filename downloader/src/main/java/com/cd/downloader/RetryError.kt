package com.cd.downloader


class RetryError : Exception {
    constructor() : super("Maximum retry exceeded")
    constructor(cause: Throwable?) : super(cause)
}