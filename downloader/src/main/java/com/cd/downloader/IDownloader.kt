package com.cd.downloader

interface IDownloader {
    fun add(request: DownloadRequest): Int
    fun cancel(downloadId: Int): Int
    fun cancelAll()
    fun pause(downloadId: Int): Int
    fun pauseAll()
    fun query(downloadId: Int): Int
    fun release()
    val isReleased: Boolean

    companion object {
        /**
         * Status when the download is currently pending.
         */
        const val STATUS_PENDING = 1 shl 0

        /**
         * Status when the download is currently pending.
         */
        const val STATUS_STARTED = 1 shl 1

        /**
         * Status when the download network call is connecting to destination.
         */
        const val STATUS_CONNECTING = 1 shl 2

        /**
         * Status when the download is currently running.
         */
        const val STATUS_RUNNING = 1 shl 3

        /**
         * Status when the download has successfully completed.
         */
        const val STATUS_SUCCESSFUL = 1 shl 4

        /**
         * Status when the download has failed.
         */
        const val STATUS_FAILED = 1 shl 5

        /**
         * Status when the download has failed due to broken url or invalid download url
         */
        const val STATUS_NOT_FOUND = 1 shl 6

        /**
         * Status when the download is attempted for retry due to connection timeouts.
         */
        const val STATUS_RETRYING = 1 shl 7

        /**
         * Error code when writing download content to the destination file.
         */
        const val ERROR_FILE_ERROR = 1001

        /**
         * Error code when an HTTP code was received that download manager can't
         * handle.
         */
        const val ERROR_UNHANDLED_HTTP_CODE = 1002

        /**
         * Error code when an error receiving or processing data occurred at the
         * HTTP level.
         */
        const val ERROR_HTTP_DATA_ERROR = 1004

        /**
         * Error code when there were too many redirects.
         */
        const val ERROR_TOO_MANY_REDIRECTS = 1005

        /**
         * Error code when size of the file is unknown.
         */
        const val ERROR_DOWNLOAD_SIZE_UNKNOWN = 1006

        /**
         * Error code when passed URI is malformed.
         */
        const val ERROR_MALFORMED_URI = 1007

        /**
         * Error code when download is cancelled.
         */
        const val ERROR_DOWNLOAD_CANCELLED = 1008

        /**
         * Error code when there is connection timeout after maximum retries
         */
        const val ERROR_CONNECTION_TIMEOUT_AFTER_RETRIES = 1009
    }
}