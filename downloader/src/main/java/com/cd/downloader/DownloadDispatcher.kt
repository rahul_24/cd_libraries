package com.cd.downloader

import android.content.ContentValues
import android.os.Process
import com.cd.downloader.DownloadRequestQueue.CallBackDelivery
import com.cd.downloader.util.Log.d
import com.cd.downloader.util.Log.v
import org.apache.http.conn.ConnectTimeoutException
import java.io.*
import java.net.*
import java.util.*
import java.util.concurrent.BlockingQueue

/**
 * This thread/class used to make [HttpURLConnection], receives Response from server
 * and Dispatch the response to respective [DownloadRequest]
 */
internal class DownloadDispatcher
/**
 * Constructor take the dependency (DownloadRequest queue) that all the Dispatcher needs
 */(
    /**
     * The queue of download requests to service.
     */
    private val mQueue: BlockingQueue<DownloadRequest>?,
    /**
     * To Delivery call back response on main thread
     */
    private val mDelivery: CallBackDelivery?
) : Thread() {
    /**
     * The buffer size used to stream the data
     */
    private val BUFFER_SIZE = 4096

    /**
     * The maximum number of redirects.
     */
    private val MAX_REDIRECTS = 5 // can't be more than 7.
    private val HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416
    private val HTTP_TEMP_REDIRECT = 307

    /**
     * Used to tell the dispatcher to die.
     */
    @Volatile
    private var mQuit = false

    /**
     * How many times redirects happened during a download request.
     */
    private var mRedirectionCount = 0
    private var mContentLength: Long = 0
    private var shouldAllowRedirects = true

    /**
     * This variable is part of resumable download feature.
     * It will load the downloaded file cache length, If It had been already in available Downloaded Requested output path.
     * Otherwise it would keep "0" always by default.
     */
    private var mDownloadedCacheSize: Long = 0
    private var mTimer: Timer? = null

    override fun run() {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND)
        mTimer = Timer()
        while (true) {
            var request: DownloadRequest? = null
            try {
                request = mQueue!!.take()
                mRedirectionCount = 0
                shouldAllowRedirects = true
                v("Download initiated for " + request.downloadId)
                updateDownloadState(request, IDownloader.STATUS_STARTED)
                executeDownload(request, request.uri.toString())
            } catch (e: InterruptedException) {
                // We may have been interrupted because it was time to quit.
                if (mQuit) {
                    if (request != null) {
                        request.finish()
                        // don't remove files that have been downloaded successfully.
                        if (request.downloadState != IDownloader.STATUS_SUCCESSFUL) {
                            updateDownloadFailed(
                                request,
                                IDownloader.ERROR_DOWNLOAD_CANCELLED,
                                "Download cancelled"
                            )
                        }
                    }
                    mTimer!!.cancel()
                    return
                }
            }
        }
    }

    fun quit() {
        mQuit = true
        interrupt()
    }

    private fun executeDownload(request: DownloadRequest, downloadUrl: String) {
        val url: URL = try {
            URL(downloadUrl)
        } catch (e: MalformedURLException) {
            updateDownloadFailed(
                request,
                IDownloader.ERROR_MALFORMED_URI,
                "MalformedURLException: URI passed is malformed."
            )
            return
        }
        var conn: HttpURLConnection? = null
        try {
            conn = url.openConnection() as HttpURLConnection
            val destinationFile = File(request.destinationURI.path!!)
            if (destinationFile.exists()) {
                mDownloadedCacheSize = destinationFile.length()
            }
            d(ContentValues.TAG, "Existing file mDownloadedCacheSize: $mDownloadedCacheSize")
            conn.setRequestProperty(
                "Range",
                "bytes=$mDownloadedCacheSize-"
            )    //Request property to Resume downloading
            conn.instanceFollowRedirects = false
            conn.connectTimeout = request.retryPolicy.currentTimeout
            conn.readTimeout = request.retryPolicy.currentTimeout
            val customHeaders = request.customHeaders
            customHeaders.keys.forEach { headerName ->
                conn.addRequestProperty(
                    headerName,
                    customHeaders[headerName]
                )
            }

            // Status Connecting is set here before
            // urlConnection is trying to connect to destination.
            updateDownloadState(request, IDownloader.STATUS_CONNECTING)
            val responseCode = conn.responseCode
            v("Response code obtained for downloaded Id " + request.downloadId + " : httpResponse Code " + responseCode)

            when (responseCode) {
                HttpURLConnection.HTTP_PARTIAL, HttpURLConnection.HTTP_OK -> {
                    shouldAllowRedirects = false
                    if (readResponseHeaders(request, conn, responseCode) == 1) {
                        d(ContentValues.TAG, "Existing mDownloadedCacheSize: $mDownloadedCacheSize")
                        d(ContentValues.TAG, "File mContentLength: $mContentLength")
                        if (mDownloadedCacheSize == mContentLength) { // Mark as success, If end of stream already reached
                            updateDownloadComplete(request)
                            d(ContentValues.TAG, "Download Completed")
                        } else {
                            transferData(request, conn)
                        }
                    } else {
                        updateDownloadFailed(
                            request,
                            IDownloader.ERROR_DOWNLOAD_SIZE_UNKNOWN,
                            "Transfer-Encoding not found as well as can't know size of download, giving up"
                        )
                    }
                    return
                }
                HttpURLConnection.HTTP_MOVED_PERM, HttpURLConnection.HTTP_MOVED_TEMP, HttpURLConnection.HTTP_SEE_OTHER, HTTP_TEMP_REDIRECT -> {
                    // Take redirect url and call executeDownload recursively until
                    // MAX_REDIRECT is reached.
                    while (mRedirectionCount < MAX_REDIRECTS && shouldAllowRedirects) {
                        mRedirectionCount++
                        v(ContentValues.TAG, "Redirect for downloaded Id " + request.downloadId)
                        val location = conn.getHeaderField("Location")
                        executeDownload(request, location)
                    }
                    if (mRedirectionCount > MAX_REDIRECTS && shouldAllowRedirects) {
                        updateDownloadFailed(
                            request,
                            IDownloader.ERROR_TOO_MANY_REDIRECTS,
                            "Too many redirects, giving up"
                        )
                        return
                    }
                }
                HTTP_REQUESTED_RANGE_NOT_SATISFIABLE -> updateDownloadFailed(
                    request,
                    HTTP_REQUESTED_RANGE_NOT_SATISFIABLE,
                    conn.responseMessage
                )
                HttpURLConnection.HTTP_UNAVAILABLE -> updateDownloadFailed(
                    request,
                    HttpURLConnection.HTTP_UNAVAILABLE,
                    conn.responseMessage
                )
                HttpURLConnection.HTTP_INTERNAL_ERROR -> updateDownloadFailed(
                    request,
                    HttpURLConnection.HTTP_INTERNAL_ERROR,
                    conn.responseMessage
                )
                else -> updateDownloadFailed(
                    request,
                    IDownloader.ERROR_UNHANDLED_HTTP_CODE,
                    "Unhandled HTTP response:" + responseCode + " message:" + conn.responseMessage
                )
            }
        } catch (e: SocketTimeoutException) {
            e.printStackTrace()
            // Retry.
            attemptRetryOnTimeOutException(request)
        } catch (e: ConnectTimeoutException) {
            e.printStackTrace()
            attemptRetryOnTimeOutException(request)
        } catch (e: IOException) {
            e.printStackTrace()
            updateDownloadFailed(
                request,
                IDownloader.ERROR_HTTP_DATA_ERROR,
                "Trouble with low-level sockets"
            )
        } finally {
            conn?.disconnect()
        }
    }

    private fun transferData(request: DownloadRequest, conn: HttpURLConnection?) {
        var inputStream: BufferedInputStream? = null
        var accessFile: RandomAccessFile? = null
        cleanupDestination(request, false)

        try {
            try {
                inputStream = BufferedInputStream(conn!!.inputStream)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            val destinationFile = File(request.destinationURI.path!!)
            var errorCreatingDestinationFile = false
            // Create destination file if it doesn't exists
            if (!destinationFile.exists()) {
                try {
                    mDownloadedCacheSize = 0

                    // Check path
                    val parentPath = destinationFile.parentFile
                    if (parentPath != null && !parentPath.exists()) {
                        parentPath.mkdirs()
                    }
                    if (!destinationFile.createNewFile()) {
                        errorCreatingDestinationFile = true
                        updateDownloadFailed(
                            request, IDownloader.ERROR_FILE_ERROR,
                            "Error in creating destination file"
                        )
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                    errorCreatingDestinationFile = true
                    updateDownloadFailed(
                        request, IDownloader.ERROR_FILE_ERROR,
                        "Error in creating destination file"
                    )
                }
            } else {
                if (inputStream != null) {
                    request.abortCancel()
                }
            }

            // If Destination file couldn't be created. Abort the data transfer.
            if (!errorCreatingDestinationFile) {
                try {
                    accessFile = RandomAccessFile(destinationFile, "rw")
                    accessFile.seek(mDownloadedCacheSize)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                when {
                    inputStream == null -> {
                        updateDownloadFailed(
                            request,
                            IDownloader.ERROR_FILE_ERROR,
                            "Error in creating input stream"
                        )
                    }
                    accessFile == null -> {
                        updateDownloadFailed(
                            request,
                            IDownloader.ERROR_FILE_ERROR,
                            "Error in writing download contents to the destination file"
                        )
                    }
                    else -> {
                        // Start streaming data
                        transferData(request, inputStream, accessFile)
                    }
                }
            }
        } finally {
            try {
                inputStream?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            try {
                accessFile?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun transferData(
        request: DownloadRequest,
        inputStream: InputStream,
        out: RandomAccessFile
    ) {
        val data = ByteArray(BUFFER_SIZE)
        var mCurrentBytes = mDownloadedCacheSize
        request.downloadState = IDownloader.STATUS_RUNNING
        v("Content Length: " + mContentLength + " for Download Id " + request.downloadId)
        while (true) {
            if (request.isCancelled) {
                v("Stopping the download as Download Request is cancelled for Downloaded Id " + request.downloadId)
                request.finish()
                updateDownloadFailed(
                    request,
                    IDownloader.ERROR_DOWNLOAD_CANCELLED,
                    "Download cancelled"
                )
                return
            }
            val bytesRead = readFromResponse(request, data, inputStream)
            if (mContentLength > 0) {
                val progress = (mCurrentBytes * 100 / mContentLength).toInt()
                updateDownloadProgress(request, progress, mCurrentBytes)
            }
            if (bytesRead == -1) { // success, end of stream already reached
                updateDownloadComplete(request)
                return
            } else if (bytesRead == Int.MIN_VALUE) {
                return
            }
            mCurrentBytes += if (writeDataToDestination(request, data, bytesRead, out)) {
                bytesRead.toLong()
            } else {
                request.finish()
                updateDownloadFailed(
                    request,
                    IDownloader.ERROR_FILE_ERROR,
                    "Failed writing file"
                )
                return
            }
        }
    }

    private fun readFromResponse(
        request: DownloadRequest,
        data: ByteArray,
        entityStream: InputStream
    ): Int {
        return try {
            entityStream.read(data)
        } catch (ex: IOException) {
            if ("unexpected end of stream" == ex.message) {
                return -1
            }
            updateDownloadFailed(
                request,
                IDownloader.ERROR_HTTP_DATA_ERROR,
                "IOException: Failed reading response"
            )
            Int.MIN_VALUE
        }
    }

    private fun writeDataToDestination(
        request: DownloadRequest,
        data: ByteArray,
        bytesRead: Int,
        out: RandomAccessFile
    ): Boolean {
        var successInWritingToDestination = true
        try {
            out.write(data, 0, bytesRead)
        } catch (ex: IOException) {
            updateDownloadFailed(
                request,
                IDownloader.ERROR_FILE_ERROR,
                "IOException when writing download contents to the destination file"
            )
            successInWritingToDestination = false
        } catch (e: Exception) {
            updateDownloadFailed(
                request,
                IDownloader.ERROR_FILE_ERROR,
                "Exception when writing download contents to the destination file"
            )
            successInWritingToDestination = false
        }
        return successInWritingToDestination
    }

    private fun readResponseHeaders(
        request: DownloadRequest,
        conn: HttpURLConnection?,
        responseCode: Int
    ): Int {
        val transferEncoding = conn!!.getHeaderField("Transfer-Encoding")
        mContentLength = -1
        if (transferEncoding == null) {
            mContentLength = if (responseCode == HttpURLConnection.HTTP_OK) {
                // If file download already completed, 200 HttpStatusCode will thrown by service.
                getHeaderFieldLong(conn, "Content-Length", -1)
            } else {
                // If file download already partially completed, 206 HttpStatusCode will thrown by service and we can resume remaining chunks downloads.
                getHeaderFieldLong(conn, "Content-Length", -1) + mDownloadedCacheSize
            }
        } else {
            v("Ignoring Content-Length since Transfer-Encoding is also defined for Downloaded Id " + request.downloadId)
        }
        return if (mContentLength != -1L) {
            1
        } else if (transferEncoding == null || !transferEncoding.equals(
                "chunked",
                ignoreCase = true
            )
        ) {
            -1
        } else {
            1
        }
    }

    private fun getHeaderFieldLong(conn: URLConnection, field: String, defaultValue: Long): Long {
        return try {
            conn.getHeaderField(field).toLong()
        } catch (e: NumberFormatException) {
            defaultValue
        }
    }

    private fun attemptRetryOnTimeOutException(request: DownloadRequest) {
        updateDownloadState(request, IDownloader.STATUS_RETRYING)
        val retryPolicy = request.retryPolicy
        try {
            retryPolicy.retry()
            mTimer?.schedule(object : TimerTask() {
                override fun run() {
                    executeDownload(request, request.uri.toString())
                }
            }, retryPolicy.currentTimeout.toLong())
        } catch (e: RetryError) {
            // Update download failed.
            updateDownloadFailed(
                request, IDownloader.ERROR_CONNECTION_TIMEOUT_AFTER_RETRIES,
                "Connection time out after maximum retires attempted"
            )
        }
    }

    /**
     * Called just before the thread finishes, regardless of status, to take any necessary action on
     * the downloaded file with mDownloadedCacheSize file.
     *
     * @param forceClean -  It will delete downloaded cache, Even streaming is enabled, If user intentionally cancelled.
     */
    private fun cleanupDestination(request: DownloadRequest, forceClean: Boolean) {
        if (!request.isResumable || forceClean) {
            d("cleanupDestination() deleting " + request.destinationURI.path)
            val destinationFile = File(request.destinationURI.path!!)
            if (destinationFile.exists()) {
                destinationFile.delete()
            }
        }
    }

    private fun updateDownloadState(request: DownloadRequest, state: Int) {
        request.downloadState = state
    }

    private fun updateDownloadComplete(request: DownloadRequest) {
        mDownloadedCacheSize = 0 // reset into Zero.
        mDelivery!!.postDownloadComplete(request)
        request.downloadState = IDownloader.STATUS_SUCCESSFUL
        request.finish()
    }

    private fun updateDownloadFailed(request: DownloadRequest, errorCode: Int, errorMsg: String) {
        mDownloadedCacheSize = 0 // reset into Zero.
        shouldAllowRedirects = false
        request.downloadState = IDownloader.STATUS_FAILED
        if (request.deleteDestinationFileOnFailure) {
            cleanupDestination(request, true)
        }
        mDelivery!!.postDownloadFailed(request, errorCode, errorMsg)
        request.finish()
    }

    private fun updateDownloadProgress(
        request: DownloadRequest,
        progress: Int,
        downloadedBytes: Long
    ) {
        mDelivery!!.postProgressUpdate(request, mContentLength, downloadedBytes, progress)
    }
}